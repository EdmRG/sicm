var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;


  var path = require('path')
  //var cors = require('cors')
  //app.use(cors())
  var gemail = ''
  var gpassword = ''
  var gnombre = ''
  var gapellido = ''
  var gnumcli = ''


  app.use(express.static(__dirname + '/build/default'));

var requestjson = require('request-json')
var bodyparser = require('body-parser')
app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var urlClientes = "https://api.mlab.com/api/1/databases/erodriguezg/collections/clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMLab = requestjson.createClient(urlClientes)

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/erodriguezg/collections/"
var apiKey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMlabRaiz;

var movimientosJSON = require('./movimientosv2.json');
//var clienteMLab = requestjson.createClient(urlClientes)

app.listen(port);
console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req,res) {
  // res.send("Hola mundo");
   res.sendFile(path.join(__dirname, 'index.html'))
})
app.post('/', function(req,res) {
   res.send("Hemos recibido su peticion post");
})

app.put('/', function(req,res) {
   res.send("Hemos recibido su peticion put");
})

app.delete('/', function(req,res) {
   res.send("Hemos recibido su peticion delete");
})

app.get('/Clientes/:idcliente', function(req,res) {
    res.send("Aquí tiene al cliente número: " + req.params.idcliente);
})

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function(req, res) {
  res.sendfile(movimientosJSON);
})

app.get('/v2/Movimientos/:idcliente', function(req,res) {
    //res.send(movimientosJSON[req.params.idcliente-1].importe );
    res.send(movimientosJSON[req.params.idcliente-1]);
})
app.get('/v2/movimientosquery', function(req,res) {
    res.send(req.query);
})

app.post('v2/Movimientos', function(req, res) {
   var nuevo = req.body
   nuevo.id = movimientosJSON.length + 1
   movimientosJSON.push(nuevo)
   res.send("Movimiento dado de alta")
})

function posicion (cadena, valor) {
  //   [{"_id":{"$oid":"5f0560b3b7099950c6a15432"},"email":"edmundo@bbva.com","password":"1234","numcli":"12345"}]
  //console.log(cadena)
  //console.log(cadena.length)
  //console.log(valor)
     var posini=0
     var posfin=0
     var res=''
     for (var i=0;i<cadena.length;i++) {
      if (cadena[i] === '"' ) {
         posini=i+1
         //console.log(i)
         for (var j=i+1;j<cadena.length;j++) {
             if (cadena[j] === '"' )  {
                posfin = j
                //console.log(posfin)
                if (cadena.substring(posini,posfin) === valor) {
                  //console.log("si entro "+valor)
                  for (var k=posfin+1;k<cadena.length;k++) {
                   if (cadena[k] === '"' ) {
                      posini = k+1
                      //console.log("valor posini "+posini)
                      for (var l=k+1;l<cadena.length;l++) {
                          if (cadena[l] === '"' )  {
                             posfin = l
                             res = cadena.substring(posini,posfin)
                             posini=0
                             posfin=0
                             l=cadena.length
                             j=l
                          }
                      }
                      k=l
                      l=cadena.length

                    }
                  }
                }
                i=j
                j=cadena.length
                //i=j
             }
           }
      }
       //console.log(userLogin[i].nombre + " "+userLogin[i].numcli)
     }
 return (res);
}

app.get('/Usuarios', function(req, res) {
  var email = gemail;
  var password = gpassword;

  var query = 'q={"email":"'+email+'","password":"'+password+'"}'
  //console.log(query);
  //var query = '';
  clienteMLab = requestjson.createClient(urlMlabRaiz + "usuarios" + apiKey + "&" + query)
  //console.log(urlMlabRaiz + "Usuarios" + apiKey + "&" + query)

  clienteMLab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body);
      //console.log("Llamado a Usuario -> ");
      //gnumcli = body.numcli
      //console.log(gnumcli)
    } else {
      console.log(body);
    }
  })
})

app.get('/Movto', function(req, res) {
  var id = this.id
  var query = 'q={"_id":"'+id+'"}'
  clienteMLab = requestjson.createClient(urlMlabRaiz + "clientes" + apiKey + "&" + query)
  console.log(urlMlabRaiz + "Clientes" + apiKey + "&" + query)

  clienteMLab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body);
         //console.log(body);
    } else {
      console.log("Error");
    }
  })
})

app.get('/Clientes', function(req, res) {
  var numcli = gnumcli
  var query = 'q={"numcli":"'+numcli+'"}'
  clienteMLab = requestjson.createClient(urlMlabRaiz + "clientes" + apiKey + "&" + query)
  //console.log(urlMlabRaiz + "Clientes" + apiKey + "&" + query)

  clienteMLab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body);
         //console.log(body);
    } else {
      console.log("Error");
    }
  })
})


app.post('/DelMov', function(req, res) {
  var numcli = gnumcli
  var id = req.body.id
  var fecha = req.body.fecha
  var importe = req.body.importe
  var tipo = req.body.tipo
  var query = '{"_id":"'+id+'"}'
  //console.log(query)
  clienteMLab = requestjson.createClient(urlMlabRaiz + "clientes/" + id + apiKey)
  //console.log(clienteMLab)

  clienteMLab.delete('', function(err, resM, body) {
    res.send({
       "msg":"Registro borrado"

    })
   })
 })

app.post('/AddMov', function(req, res) {
  var numcli = gnumcli
   let newRegister = {
       "numcli": numcli,
       "producto":  req.body.producto,
       "fecha": req.body.fecha,
       "importe": req.body.importe,
       "tipo": req.body.tipo
   };

  //console.log(newRegister)
  clienteMLab = requestjson.createClient(urlMlabRaiz + "clientes" + apiKey)
  //console.log(clienteMLab)
  //clienteMLab = requestjson.createClient(urlMlabRaiz + "clientes" + apiKey + "&u=true,  " + query)
  //console.log(clienteMLab)

  clienteMLab.post(urlMlabRaiz + "clientes" + apiKey, newRegister, function(err, resM, body) {
    res.send({
       "msg":"Registro ingresado"

    })
  })
})


app.post('/UpdateMov', function(req, res) {
  var numcli = gnumcli
  var id = req.body.id
  let query = {
      "numcli": numcli,
      "producto":  req.body.producto,
      "fecha": req.body.fecha,
      "importe": req.body.importe,
      "tipo": req.body.tipo
  };
  let object = { findAndModify: '<collection>',
     // use your own collection name
     "query": id, update: { $set: query }, new: true }

  let objeto2 = JSON.stringify(object);
  //clienteMLab = requestjson.createClient(urlMlabRaiz + "clientes/" + id + apiKey + ", " + query)
  //console.log(clienteMLab)
  //console.log(urlMlabRaiz + "clientes" + apiKey + '&q={"_id":"'+id+'"}')
  //clienteMLab = requestjson.createClient(urlMlabRaiz + "clientes/" + id + apiKey)
   console.log(urlMlabRaiz + "clientes/"+id+apiKey)

  //clienteMLab.post(urlMlabRaiz + "clientes"+apiKey, objeto2, function(err, resM, body) {
  //clienteMLab.post(urlMlabRaiz + "clientes/"+id+apiKey, '"$set:"' + query, function(err, resM, body) {
  clienteMLab.put(urlMlabRaiz + "clientes/"+id+apiKey, query, function(err, resM, body) {
    console.log(res.send())
    res.send({
       "msg":"Registro actualizado"
    })
  })
})


app.post('/Login', function(req, res) {
   //res.set("Access-Control-Allow-Headers", "Contenct-Type")
   var email = req.body.email
   var password = req.body.password

   gemail = email;
   gpassword = password;

   var query = 'q={"email":"'+email+'","password":"'+password+'"}'
   clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "usuarios" + apiKey + "&" + query)
   //console.log(clienteMlabRaiz)

   clienteMlabRaiz.get('', function(err, resM, body)  {
     //res.send(body);   marca error
     if (!err) {
        if (body.length == 1) {
           var userLogin = JSON.stringify(body);
           gnumcli = posicion(userLogin, "numcli" )
           gnombre = posicion(userLogin, "nombre" )
           gapellido = posicion(userLogin, "apellido" )
           //console.log(gnumcli)
           //console.log(gnombre)
           //console.log(gapellido)
           res.status(200).send("Usuario logado")
           console.log("Usuario logado")
        } else {
           res.status(404).send('Usuario no encontrado')
           console.log("Usuario NO encontrado")
        }
      } else {
        console.log(err)
      }
    })

 })
